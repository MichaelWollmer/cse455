package com.example.lab02;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText editText01;
    private Button bnt01;
    private TextView textView01;
    private String USD;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText01=findViewById(R.id.EditText01);
        bnt01=findViewById(R.id.bnt);
        textView01=findViewById(R.id.Yen);

        bnt01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertToYen) {
                USD=editText01.getText().toString();
                if(USD.equals("")){
                    textView01.setText("This Field Cannot be Left Blank");
                }
                else {
                    Double dInputs = Double.parseDouble(USD);
                    Double result = dInputs * 112.57;
                    textView01.setText("$" + USD + "=" + String.format("%.2f", result));
                    editText01.setText("");
                }

            }
        });
    }
}
